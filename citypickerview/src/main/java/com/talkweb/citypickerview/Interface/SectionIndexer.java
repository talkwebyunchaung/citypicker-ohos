package com.talkweb.citypickerview.Interface;

public interface SectionIndexer {
    Object[] getSections();

    int getPositionForSection(int sectionIndex);

    int getSectionForPosition(int position);
}
