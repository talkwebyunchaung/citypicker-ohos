package com.talkweb.citypickerview;

/**
 * 作者：liji on 2018/1/22 09:46
 * 邮箱：lijiwork@sina.com
 * QQ ：275137657
 */

public class Constant {

    private Constant() {
    }

    public static final String CITY_DATA = "china_city_data.json";

    public static final int RESULT_OK = -1;
}
