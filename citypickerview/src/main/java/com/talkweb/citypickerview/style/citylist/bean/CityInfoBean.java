package com.talkweb.citypickerview.style.citylist.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 作者：liji on 2017/5/19 17:07
 * 邮箱：lijiwork@sina.com
 * QQ ：275137657
 */

public class CityInfoBean implements Serializable {

    
    private String id; /*110101*/
    
    private String name; /*东城区*/
    
    private ArrayList<CityInfoBean> cityList;
    
    public ArrayList<CityInfoBean> getCityList() {
        return cityList;
    }
    
    public void setCityList(ArrayList<CityInfoBean> cityList) {
        this.cityList = cityList;
    }
    
    public CityInfoBean() {
    }
    
    public static CityInfoBean findCity(List<CityInfoBean> list, String cityName) {
        try {
            for (int i = 0; i < list.size(); i++) {
                CityInfoBean city = list.get(i);
                if (cityName.equals(city.getName()) /*|| cityName.contains(city.getName())
                        || city.getName().contains(cityName)*/) {
                    return city;
                }
            }
        }
        catch (Exception e) {
            return null;
        }
        return null;
    }


    public String getId() {
        return id == null ? "" : id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }


    protected CityInfoBean(String id, String name, ArrayList<CityInfoBean> cityList) {
        this.id = id;
        this.name = name;
        this.cityList = cityList;
    }

}
