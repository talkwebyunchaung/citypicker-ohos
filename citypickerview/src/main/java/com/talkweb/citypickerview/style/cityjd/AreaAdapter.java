package com.talkweb.citypickerview.style.cityjd;

import com.talkweb.citypickerview.ResourceTable;
import com.talkweb.citypickerview.bean.DistrictBean;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;


/**
 * 作者：liji on 2018/1/29 17:01
 * 邮箱：lijiwork@sina.com
 * QQ ：275137657
 */

public class AreaAdapter extends BaseItemProvider {

    Context context;

    List<DistrictBean> mDistrictList;

    private int districtIndex = JDConst.INDEX_INVALID;

    public AreaAdapter(Context context, List<DistrictBean> mDistrictList) {
        this.context = context;
        this.mDistrictList = mDistrictList;
    }


    public int getSelectedPosition() {
        return this.districtIndex;
    }

    public void updateSelectedPosition(int index) {
        this.districtIndex = index;
    }

    @Override
    public int getCount() {
        return mDistrictList.size();
    }

    @Override
    public DistrictBean getItem(int position) {
        return mDistrictList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return Long.parseLong(mDistrictList.get(position).getId());
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        Holder holder;

        if (convertView == null) {
            convertView = LayoutScatter.getInstance(parent.getContext()).parse(ResourceTable.Layout_pop_jdcitypicker_item, parent, false);

            holder = new Holder();
            holder.name = (Text) convertView.findComponentById(ResourceTable.Id_name);
            holder.selectImg = (Image) convertView.findComponentById(ResourceTable.Id_selectImg);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        DistrictBean item = getItem(position);
        holder.name.setText(item.getName());

        boolean checked = districtIndex != JDConst.INDEX_INVALID && mDistrictList.get(districtIndex).getName().equals(item.getName());
        holder.name.setEnabled(!checked);
        holder.selectImg.setVisibility(checked ? Component.VISIBLE : Component.HIDE);


        return convertView;
    }


    class Holder {
        Text name;
        Image selectImg;
    }
}
