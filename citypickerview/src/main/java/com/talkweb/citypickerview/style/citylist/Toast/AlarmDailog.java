package com.talkweb.citypickerview.style.citylist.Toast;

import com.talkweb.citypickerview.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;


/**
 * author：admin on 2016/5/4 10:50
 * email：fangkaijin@gmail.com
 */
public class AlarmDailog extends ToastDialog
{
    private ToastDialog toast;
    private Text noticeText;


    public AlarmDailog(Context context)
    {
        super(context);
        LayoutScatter inflater = LayoutScatter.getInstance(context);
        Component layout = inflater.parse(ResourceTable.Layout_dialog_alarm_ui, null, false);
        noticeText = (Text) layout.findComponentById( ResourceTable.Id_noticeText);
        toast = new ToastDialog(context);
        toast.setAlignment(TextAlignment.CENTER);
        //让Toast显示为我们自定义的样子
        toast.setComponent(layout);
    }

    public void setShowText(String dialogNotice)
    {
        noticeText.setText(dialogNotice);
    }

    @Override
    public void show()
    {
        toast.show();
    }

}
