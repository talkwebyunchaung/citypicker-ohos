package com.talkweb.citypickerview.style.citythreelist;

import ohos.utils.Parcel;

import java.io.Serializable;

/**
 * 作者：liji on 2018/3/20 10:57
 * 邮箱：lijiwork@sina.com
 * QQ ：275137657
 */

public class CityBean implements Serializable {
    private String id; /*110101*/
    
    private String name; /*东城区*/
    
    public String getId() {
        return id == null ? "" : id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getName() {
        return name == null ? "" : name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public CityBean() {
    }
    
    protected CityBean(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
    }

}
