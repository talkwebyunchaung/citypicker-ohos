package com.talkweb.citypickerview.style.citythreelist;

import com.talkweb.citypickerview.Constant;
import com.talkweb.citypickerview.ResourceTable;
import com.talkweb.citypickerview.style.citylist.bean.CityInfoBean;
import com.talkweb.citypickerview.style.citylist.utils.CityListLoader;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;

import java.util.List;

/**
 * 选择省份三级列表
 */
public class ProvinceActivity extends Ability {
    
    private Text mCityNameTv;
    
    private ListContainer mCityRecyclerView;
    
    public static final int RESULT_DATA = 1001;
    
    private CityBean provinceBean = new CityBean();

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_citylist);

        initView();
        setData();
    }
    
    private void setData() {
        
        final List<CityInfoBean> cityList = CityListLoader.getInstance().getProListData();
        if (cityList == null) {
            return;
        }
        
        CityAdapter cityAdapter = new CityAdapter(ProvinceActivity.this, cityList);
        mCityRecyclerView.setItemProvider(cityAdapter);
        cityAdapter.setOnItemClickListener((view, position) -> {

            provinceBean.setId(cityList.get(position).getId());
            provinceBean.setName(cityList.get(position).getName());
            Intent intent = new Intent();
            intent.setElementName(getBundleName(), CityActivity.class);
            intent.setParam(CityListLoader.BUNDATA, cityList.get(position));
            startAbilityForResult(intent, RESULT_DATA);
        });
        
    }
    
    private void initView() {
        mCityNameTv = (Text) findComponentById(ResourceTable.Id_cityname_tv);
        mCityNameTv.setText("选择省份");
        mCityRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_city_recyclerview);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent data) {
        super.onAbilityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_DATA && data != null) {
            CityBean area = data.getSerializableParam("area");
            CityBean city = data.getSerializableParam("city");
            Intent intent = new Intent();
            intent.setParam("province", provinceBean);
            intent.setParam("city", city);
            intent.setParam("area", area);
            setResult(Constant.RESULT_OK, intent);
            terminateAbility();
        }
    }
    
}
