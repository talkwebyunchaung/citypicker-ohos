package com.talkweb.citypickerview.style.citythreelist;

import com.talkweb.citypickerview.Constant;
import com.talkweb.citypickerview.ResourceTable;
import com.talkweb.citypickerview.style.citylist.bean.CityInfoBean;
import com.talkweb.citypickerview.style.citylist.utils.CityListLoader;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;

import java.util.List;


public class CityActivity extends Ability {
    
    private Text mCityNameTv;
    
    private Image mImgBack;
    
    private ListContainer mCityRecyclerView;
    
    private CityInfoBean mProInfo = null;

    private CityBean cityBean = new CityBean();
    
    private CityBean area = new CityBean();

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_citylist);

        mProInfo = this.getIntent().getSerializableParam(CityListLoader.BUNDATA);
        initView();

        setData(mProInfo);
    }
    
    private void setData(CityInfoBean mProInfo) {
        
        if (mProInfo != null && mProInfo.getCityList().size() > 0) {
            mCityNameTv.setText("" + mProInfo.getName());
            
            final List<CityInfoBean> cityList = mProInfo.getCityList();
            if (cityList == null) {
                return;
            }
            
            CityAdapter cityAdapter = new CityAdapter(CityActivity.this, cityList);
            mCityRecyclerView.setItemProvider(cityAdapter);
            cityAdapter.setOnItemClickListener((view, position) -> {

                cityBean.setId(cityList.get(position).getId());
                cityBean.setName(cityList.get(position).getName());

                Intent intent = new Intent();
                intent.setElementName(getBundleName(), AreaActivity.class);
                intent.setParam(CityListLoader.BUNDATA, cityList.get(position));
                startAbilityForResult(intent, ProvinceActivity.RESULT_DATA);
            });
            
        }
    }
    
    private void initView() {
        mImgBack = (Image) findComponentById(ResourceTable.Id_img_left);
        mCityNameTv = (Text) findComponentById(ResourceTable.Id_cityname_tv);
        mImgBack.setVisibility(Component.VISIBLE);
        mImgBack.setClickedListener(v -> terminateAbility());

        mCityRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_city_recyclerview);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent data) {
        super.onAbilityResult(requestCode, resultCode, data);
        if (resultCode == ProvinceActivity.RESULT_DATA && data != null) {
            area = data.getSerializableParam("area");
            Intent intent = new Intent();
            intent.setParam("city", cityBean);
            intent.setParam("area", area);
            setResult(Constant.RESULT_OK, intent);
            terminateAbility();
        }
    }

}
