package com.talkweb.citypickerview.style.citythreelist;

import com.talkweb.citypickerview.ResourceTable;
import com.talkweb.citypickerview.style.citylist.bean.CityInfoBean;
import com.talkweb.citypickerview.style.citylist.utils.CityListLoader;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;

import java.util.List;

import static com.talkweb.citypickerview.style.citythreelist.ProvinceActivity.RESULT_DATA;


public class AreaActivity extends Ability {
    
    private Text mCityNameTv;
    
    private Image mImgBack;
    
    private ListContainer mCityRecyclerView;
    
    private CityInfoBean mProCityInfo = null;
    
    private CityBean areaBean = new CityBean();

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_citylist);
        mProCityInfo = this.getIntent().getSerializableParam(CityListLoader.BUNDATA);
        initView();

        setData();
    }
    
    private void setData() {
        if (mProCityInfo != null && mProCityInfo.getCityList().size() > 0) {
            mCityNameTv.setText("" + mProCityInfo.getName());
            
            final List<CityInfoBean> cityList = mProCityInfo.getCityList();
            if (cityList == null) {
                return;
            }
            
            CityAdapter cityAdapter = new CityAdapter(AreaActivity.this, cityList);
            mCityRecyclerView.setItemProvider(cityAdapter);
            cityAdapter.setOnItemClickListener((view, position) -> {

                areaBean.setName(cityList.get(position).getName());
                areaBean.setId(cityList.get(position).getId());

                //将计算的结果回传给第一个Activity
                Intent reReturnIntent = new Intent();
                reReturnIntent.setParam("area", areaBean);
                setResult(RESULT_DATA, reReturnIntent);
                //退出第二个Activity
                AreaActivity.this.terminateAbility();

            });
            
        }
    }
    
    private void initView() {
        mImgBack = (Image) findComponentById(ResourceTable.Id_img_left);
        mCityNameTv = (Text) findComponentById(ResourceTable.Id_cityname_tv);
        mImgBack.setVisibility(Component.VISIBLE);
        mImgBack.setClickedListener(v -> terminateAbility());
        mCityRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_city_recyclerview);

    }
    
}
