package com.talkweb.citypickerview.style.citylist.Toast;


import ohos.aafwk.ability.Ability;
import ohos.app.Context;

/**
 * author：admin on 2016/5/4 10:49
 * email：fangkaijin@gmail.com
 */
public class ToastUtils {

    private static AlarmDailog alarmDialog;

    public static
    void showShortToast (Context context, String showMsg ) {
        if ( null != alarmDialog ) {
            alarmDialog = null;
        }
        alarmDialog = new AlarmDailog(context);
        alarmDialog.setShowText(showMsg);
        alarmDialog.show();

    }

    public static void showLongToast(Context context, String showMsg)
    {
        if (null != alarmDialog)
        {
            alarmDialog = null;
        }
        alarmDialog = new AlarmDailog(context);
        alarmDialog.setShowText(showMsg);
        alarmDialog.show();
    }

    public static void showMomentToast(final Ability ability, final Context context, final String showMsg)
    {
        ability.postTask(new Runnable() {
            @Override
            public void run() {
                if (null == alarmDialog)
                {
                    alarmDialog = new AlarmDailog(context);
                    alarmDialog.setShowText(showMsg);
                    alarmDialog.show();
                }
                else
                {
                    alarmDialog.setShowText(showMsg);
                    alarmDialog.show();
                }

                ability.postTask(new Runnable() {
                    @Override
                    public void run() {
                        if(null != alarmDialog)
                        {
                            alarmDialog.cancel();
                        }
                    }
                },2000);
            }
        },0);
    }
}
