package com.talkweb.citypickerview.style.cityjd;

import com.talkweb.citypickerview.ResourceTable;
import com.talkweb.citypickerview.bean.ProvinceBean;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;


public class ProvinceAdapter extends BaseItemProvider {

    Context context;

    List<ProvinceBean> mProList;

    private int provinceIndex = JDConst.INDEX_INVALID;

    public ProvinceAdapter(Context context, List<ProvinceBean> mProList) {
        this.context = context;
        this.mProList = mProList;
    }

    public void updateSelectedPosition(int index) {
        this.provinceIndex = index;
    }

    public int getSelectedPosition() {
        return this.provinceIndex;
    }

    @Override
    public int getCount() {
        return mProList.size();
    }

    @Override
    public ProvinceBean getItem(int position) {
        return mProList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return Long.parseLong(mProList.get(position).getId());
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        Holder holder;

        if (convertView == null) {
            convertView = LayoutScatter.getInstance(parent.getContext()).parse(ResourceTable.Layout_pop_jdcitypicker_item, parent, false);

            holder = new Holder();
            holder.name = (Text) convertView.findComponentById(ResourceTable.Id_name);
            holder.selectImg = (Image) convertView.findComponentById(ResourceTable.Id_selectImg);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        ProvinceBean item = getItem(position);
        holder.name.setText(item.getName());

        boolean checked = provinceIndex != JDConst.INDEX_INVALID && mProList.get(provinceIndex).getName().equals(item.getName());
        holder.name.setEnabled(!checked);
        holder.selectImg.setVisibility(checked ? Component.VISIBLE : Component.HIDE);


        return convertView;
    }


    class Holder {
        Text name;
        Image selectImg;
    }
}
