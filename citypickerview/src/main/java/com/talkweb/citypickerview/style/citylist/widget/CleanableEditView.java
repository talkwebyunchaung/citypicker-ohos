/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.talkweb.citypickerview.style.citylist.widget;


import com.talkweb.citypickerview.utils.TextUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.components.element.Element;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 说明:
 * 作者: fangkaijin on 2017/4/10.13:27
 * 邮箱:fangkaijin@gmail.com
 */

public class CleanableEditView extends TextField implements Component.FocusChangedListener {
    /**
     * 左右两侧图片资源
     */
    private Element left, right;
    /**
     * 是否获取焦点，默认没有焦点
     */
    private boolean hasFocus = false;
    /**
     * 手指抬起时的X坐标
     */
    private int xUp = 0;

    public CleanableEditView(Context context) {
        this(context, null);
    }

    public CleanableEditView(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public CleanableEditView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        initWedgits();
    }

    /**
     * 初始化各组件
     */
    private void initWedgits() {
        try {
            left = getAroundElements()[0];
            right = getAroundElements()[2];
            initDatas();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化数据
     */
    private void initDatas() {
        try {
            // 第一次显示，隐藏删除图标
            setAroundElements(left, null ,null ,null);
            addListeners();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 添加事件监听
     */
    private void addListeners() {
        try {
            setFocusChangedListener(this);
            addTextObserver(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addTextObserver(CleanableEditView view) {
        if (hasFocus) {
            if (TextUtils.isEmpty(view.getText())) {
                // 如果为空，则不显示删除图标
                setAroundElements(left, null, null, null);
            } else {
                // 如果非空，则要显示删除图标
                if (null == right) {
                    right = getAroundElements()[2];
                }
                setAroundElements(left, null, right, null);
            }
        }
    }

    public boolean onTouchEvent(Component component, TouchEvent event) {
        try {
            switch (event.getAction()) {
                case TouchEvent.PRIMARY_POINT_UP:
                    // 获取点击时手指抬起的X坐标
                    MmiPoint mmiPoint = event.getPointerPosition(0);
                    xUp = (int) mmiPoint.getX();
                    // 当点击的坐标到当前输入框右侧的距离小于等于getCompoundPaddingRight()的距离时，则认为是点击了删除图标
                    // getCompoundPaddingRight()的说明：Returns the right padding of the view, plus space for the right Drawable if any.
                    if ((getWidth() - xUp) <= getPaddingRight() && !TextUtils.isEmpty(getText())) {
                        setText("");
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public void onFocusChange(Component v, boolean hasFocus) {
        try {
            this.hasFocus = hasFocus;
            String msg=getText().toString();

            if(hasFocus){
                if("".equalsIgnoreCase(msg)){
                    setAroundElements(left, null, null, null);
                }else{
                    setAroundElements(left, null, right, null);
                }
            }
            if(hasFocus==false){
                setAroundElements(left, null, null, null);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //获取输入内容
    public String text_String (){
        return getText().toString();
    }

}
