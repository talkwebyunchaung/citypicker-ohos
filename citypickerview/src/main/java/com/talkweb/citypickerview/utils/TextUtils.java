package com.talkweb.citypickerview.utils;

public class TextUtils {

    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }
}
