package com.talkweb.citypickerview.utils;


import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.colors.RgbColor;
import ohos.app.Context;
import ohos.global.resource.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 作者：liji on 2017/7/24 06:42
 * 邮箱：lijiwork@sina.com
 * QQ ：275137657
 */

public class utils {
    
    String cityJsonStr = "";
    
    //读取方法
    public static String getJson(Context context, String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bf = null;
        InputStreamReader in = null;
        try {
            Resource resource = context.getResourceManager()
                    .getRawFileEntry("resources/rawfile/china_city_data.json").openRawFile();
            in = new InputStreamReader(resource);
            bf = new BufferedReader(in);
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bf != null) {
                try {
                    bf.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return stringBuilder.toString();
    }
    
    public static void setBackgroundAlpha(Context mContext, float bgAlpha) {
        if (bgAlpha >= 1f) {
            clearDim(mContext);
        }else{
            applyDim(mContext, bgAlpha);
        }
    }

    private static void applyDim(Context context, float bgAlpha) {
        if (context instanceof Ability) {
            ((Ability) context).getWindow().setBackgroundColor(new RgbColor(0,0,0, (int)( bgAlpha * 255)));
        } else if (context instanceof AbilitySlice) {
            ((AbilitySlice) context).getWindow().setBackgroundColor(new RgbColor(0,0,0, (int)( bgAlpha * 255)));
        }
    }

    private static void clearDim(Context context) {
        if (context instanceof Ability) {
            ((Ability) context).getWindow().setBackgroundColor(new RgbColor(0,0,0, 0));
        } else if (context instanceof AbilitySlice) {
            ((AbilitySlice) context).getWindow().setBackgroundColor(new RgbColor(0,0,0, 0));
        }
    }
}
