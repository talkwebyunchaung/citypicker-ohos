package com.talkweb.citypickerview.bean;


import com.talkweb.citypickerview.style.citypickerview.widget.wheel.PickerData;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @2Do:
 * @Author M2
 * @Version v ${VERSION}
 * @Date 2017/7/7 0007.
 */
public class CityBean implements Serializable {
    

    private String id; /*110101*/

    @PickerData
    private String name; /*东城区*/


    private ArrayList<DistrictBean> cityList;

    @Override
    public String toString() {
        return name;
    }

    public String getId() {
        return id == null ? "" : id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<DistrictBean> getCityList() {
        return cityList;
    }

    public void setCityList(ArrayList<DistrictBean> cityList) {
        this.cityList = cityList;
    }

    public CityBean() {
    }

    protected CityBean(String id, String name) {
        this.id = id;
        this.name = name;
    }

}
