package com.talkweb.citypicker.model;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

/**
 * 作者：liji on 2017/9/20 11:20
 * 邮箱：lijiwork@sina.com
 * QQ ：275137657
 */

public class CityPickerStyleBean implements Sequenceable {
    int resourId;

    String title;

    public int getResourId() {
        return resourId;
    }

    public void setResourId(int resourId) {
        this.resourId = resourId;
    }

    public String getTitle() {
        return title == null ? "" : title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CityPickerStyleBean() {
    }

    protected CityPickerStyleBean(Parcel in) {
        this.resourId = in.readInt();
        this.title = in.readString();
    }


    public static final Sequenceable.Producer<CityPickerStyleBean> CREATOR = source -> {

        CityPickerStyleBean bean = new CityPickerStyleBean(source);
        bean.unmarshalling(source);
        return bean;
    };

    @Override
    public boolean marshalling(Parcel parcel) {
        return parcel.writeInt(this.resourId) && parcel.writeString(this.title);
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        this.resourId = parcel.readInt();
        this.title = parcel.readString();
        return true;
    }
}
