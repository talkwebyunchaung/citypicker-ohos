package com.talkweb.citypicker;

import com.talkweb.citypicker.slice.CitypickerWheelAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class CitypickerWheelAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CitypickerWheelAbilitySlice.class.getName());
    }

}
