/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.talkweb.citypicker.adapter;


import com.talkweb.citypicker.ResourceTable;
import com.talkweb.citypicker.model.CityPickerStyleBean;
import com.talkweb.citypicker.utils.SizeUtils;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import com.talkweb.library.adapter.base.BaseViewHolder;
import ohos.agp.components.*;

import java.util.List;

/**
 * 说明:
 * 工单管理list adapter
 */

public class CityPickerAdapter extends BaseQuickAdapter<CityPickerStyleBean, BaseViewHolder> {
    
    public CityPickerAdapter(int res, List<CityPickerStyleBean> datas) {
        super(res, datas);
        mData = datas;
    }

    @Override
    protected void convert(BaseViewHolder holder, CityPickerStyleBean cityPickerStyleBean) {
        int position = holder.getPosition();
        CityPickerStyleBean item = mData.get(position);

        configSize(holder.getItemView());

        Text tvName = holder.getView(ResourceTable.Id_name_tv);
        Image ivIcon = holder.getView(ResourceTable.Id_icon_img);
        tvName.setText("" + item.getTitle());
        try {
            ivIcon.setPixelMap(item.getResourId());
        } catch (Exception exception) {
            System.out.println("exception:" + exception.getLocalizedMessage());
        }

    }

    private void configSize(Component itemView) {
        ComponentContainer.LayoutConfig config = itemView.getLayoutConfig();
        config.width = SizeUtils.getScreenWidth() /3;
        config.height = SizeUtils.getScreenWidth() /3;
        itemView.setLayoutConfig(config);
    }

}
