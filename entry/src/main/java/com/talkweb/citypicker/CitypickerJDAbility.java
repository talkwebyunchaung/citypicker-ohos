package com.talkweb.citypicker;

import com.talkweb.citypicker.slice.CitypickerJDAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class CitypickerJDAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CitypickerJDAbilitySlice.class.getName());
    }
}
