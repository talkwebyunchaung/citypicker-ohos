package com.talkweb.citypicker;

import com.talkweb.citypickerview.style.citylist.utils.CityListLoader;
import com.talkweb.citypicker.utils.SizeUtils;
import ohos.aafwk.ability.AbilityPackage;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        SizeUtils.init(this);

        TaskDispatcher taskDispatcher = createParallelTaskDispatcher("city", TaskPriority.DEFAULT);
        taskDispatcher.asyncDispatch(() -> {
            /**
             * 预先加载一级列表所有城市的数据
             */
            CityListLoader.getInstance().loadCityData(MyApplication.this);

            /**
             * 预先加载三级列表显示省市区的数据
             */
            CityListLoader.getInstance().loadProData(MyApplication.this);
        });

    }
}
