package com.talkweb.citypicker;

import com.talkweb.citypicker.slice.CitypickerThreeListAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class CitypickerThreeListAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CitypickerThreeListAbilitySlice.class.getName());
    }
}
