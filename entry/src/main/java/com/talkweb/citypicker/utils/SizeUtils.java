package com.talkweb.citypicker.utils;

import ohos.agp.components.AttrHelper;
import ohos.app.AbilityContext;
import ohos.app.Context;
import ohos.global.configuration.DeviceCapability;

public class SizeUtils {

    private static Context mContext;
    private static SizeUtils instance;

    public static SizeUtils getInstance(Context context) {
        if (instance == null) {
            instance = new SizeUtils(context);
        }

        return instance;
    }

    public static void init(Context context) {
        getInstance(context);
    }

    public SizeUtils(Context context) {
        mContext = context.getApplicationContext();
    }

    public static DeviceCapability getDisplayManager() {
        return mContext.getResourceManager().getDeviceCapability();
    }

    public static int getScreenWidth() {
        return (int) vp2px(getDisplayManager().width);
    }

    public static int getScreenHeight() {
        return (int) vp2px(getDisplayManager().height);
    }

    public float fp2px(float fp) {
        return AttrHelper.fp2px(fp, mContext);
    }

    public static float vp2px(float vp) {
        return AttrHelper.vp2px(vp, mContext);
    }

}
