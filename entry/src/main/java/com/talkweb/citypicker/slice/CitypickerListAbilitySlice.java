package com.talkweb.citypicker.slice;

import com.talkweb.citypickerview.Constant;
import com.talkweb.citypickerview.style.citylist.CityListSelectAbility;
import com.talkweb.citypickerview.style.citylist.bean.CityInfoBean;
import com.talkweb.citypicker.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;

public class CitypickerListAbilitySlice extends AbilitySlice {

    Text mListTv;
    Text mResultTv;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_citypicker_list);

        findView();
    }
    private void findView() {
        Text tvTitle = (Text) findComponentById(ResourceTable.Id_tv_title);
        Image ivBack = (Image) findComponentById(ResourceTable.Id_iv_back);
        mListTv = (Text) findComponentById(ResourceTable.Id_tv_request);
        mResultTv = (Text) findComponentById(ResourceTable.Id_tv_result);

        tvTitle.setText("城市选择列表");

        ivBack.setClickedListener(component -> terminateAbility());

        mListTv.setClickedListener(v -> list());
    }

    public void list() {
        Intent intent = new Intent();
        intent.setElementName(getBundleName(), CityListSelectAbility.class);
        startAbilityForResult(intent, CityListSelectAbility.CITY_SELECT_RESULT_FRAG);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent data) {
        super.onAbilityResult(requestCode, resultCode, data);
        if (requestCode == CityListSelectAbility.CITY_SELECT_RESULT_FRAG) {
            if (resultCode == Constant.RESULT_OK) {
                if (data == null) {
                    return;
                }

                CityInfoBean cityInfoBean = (CityInfoBean) data.getSerializableParam("cityinfo");

                if (null == cityInfoBean) {
                    return;
                }

                mResultTv.setText("城市： " + cityInfoBean.getName()+"  "+cityInfoBean.getId());
            }
        }
    }

}
