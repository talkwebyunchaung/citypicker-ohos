package com.talkweb.citypicker.slice;

import com.talkweb.citypicker.*;
import com.talkweb.citypicker.adapter.CityPickerAdapter;
import com.talkweb.citypicker.model.CityPickerStyleBean;
import com.talkweb.citypicker.utils.SizeUtils;
import com.talkweb.library.adapter.base.BaseQuickAdapter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TableLayoutManager;
import ohos.agp.window.service.WindowManager;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

    ListContainer mCitypickerRv;

    CityPickerAdapter mCityPickerAdapter;

    String[] mTitle = new String[]{"城市列表", "ios选择器", "三级列表", "仿京东","自定义数据源"};

    Integer[] mIcon = new Integer[]{
            ResourceTable.Media_ic_citypicker_onecity, ResourceTable.Media_ic_citypicker_ios,
            ResourceTable.Media_ic_citypicker_three_city, ResourceTable.Media_ic_citypicker_jingdong,
            ResourceTable.Media_ic_citypicker_custome};

    private List<CityPickerStyleBean> mCityPickerStyleBeanList = new ArrayList<>();

    private void findView() {
        mCitypickerRv = (ListContainer) findComponentById(ResourceTable.Id_citypicker_rv);
    }

    private void setData() {
        for (int i = 0; i < mTitle.length; i++) {
            CityPickerStyleBean cityPickerStyleBean = new CityPickerStyleBean();
            cityPickerStyleBean.setTitle(mTitle[i]);
            cityPickerStyleBean.setResourId(mIcon[i]);
            mCityPickerStyleBeanList.add(cityPickerStyleBean);
        }
    }

    private void initRecyclerView() {
        TableLayoutManager manager = new TableLayoutManager();
        manager.setColumnCount(3);
        mCitypickerRv.setLayoutManager(manager);

        mCityPickerAdapter = new CityPickerAdapter(ResourceTable.Layout_item_citypicker, mCityPickerStyleBeanList);
        mCitypickerRv.setItemProvider(mCityPickerAdapter);
        mCityPickerAdapter.setOnItemClickListener((baseQuickAdapter, component, position) -> {
            if (null != mCityPickerAdapter && null != mCityPickerAdapter.getData()
                    && !mCityPickerAdapter.getData().isEmpty()
                    && null != mCityPickerAdapter.getData().get(position)) {
                gotoDetail(mCityPickerAdapter.getData().get(position).getResourId());
            }
        });

    }

    /**
     * 选择相关样式
     *
     * @param resourId
     */
    private void gotoDetail(int resourId) {
        switch (resourId) {
            case ResourceTable.Media_ic_citypicker_onecity:
                jumpToAbility(CitypickerListAbility.class);
                break;
            case ResourceTable.Media_ic_citypicker_ios:
                jumpToAbility(CitypickerWheelAbility.class);
                break;
            case ResourceTable.Media_ic_citypicker_three_city:
                jumpToAbility(CitypickerThreeListAbility.class);
                break;
            case ResourceTable.Media_ic_citypicker_jingdong:
                jumpToAbility(CitypickerJDAbility.class);
                break;
            case ResourceTable.Media_ic_citypicker_custome:
                jumpToAbility(CityPickerCustomeDataAbility.class);
                break;
            default:
                break;
        }
    }

    private void jumpToAbility(Class clazz) {
        Intent intent = new Intent();
        intent.setElementName(getBundleName(), clazz.getName());
        startAbility(intent);
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        findView();
        setData();
        initRecyclerView();
    }

}
