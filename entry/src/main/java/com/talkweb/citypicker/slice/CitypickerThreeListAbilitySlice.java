package com.talkweb.citypicker.slice;

import com.talkweb.citypickerview.Constant;
import com.talkweb.citypickerview.style.citythreelist.CityBean;
import com.talkweb.citypickerview.style.citythreelist.ProvinceActivity;
import com.talkweb.citypicker.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;

public class CitypickerThreeListAbilitySlice extends AbilitySlice {

    Text mListTv;

    Text mResultTv;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_citypicker_three_list);

        findView();
    }
    private void findView() {
        Text tvTitle = (Text) findComponentById(ResourceTable.Id_tv_title);
        Image ivBack = (Image) findComponentById(ResourceTable.Id_iv_back);
        mListTv = (Text) findComponentById(ResourceTable.Id_tv_request);
        mResultTv = (Text) findComponentById(ResourceTable.Id_tv_result);

        tvTitle.setText("城市选择三级列表");

        ivBack.setClickedListener(component -> terminateAbility());

        mListTv.setClickedListener(v -> list());
    }

    public void list() {
        Intent intent = new Intent();
        intent.setElementName(getBundleName(), ProvinceActivity.class);
        startAbilityForResult(intent, ProvinceActivity.RESULT_DATA);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent data) {
        super.onAbilityResult(requestCode, resultCode, data);
        if (requestCode == ProvinceActivity.RESULT_DATA) {
            if (resultCode == Constant.RESULT_OK) {
                if (data == null) {
                    return;
                }

                CityBean area = data.getSerializableParam("area");
                CityBean city = data.getSerializableParam("city");
                CityBean province = data.getSerializableParam("province");

                mResultTv.setText("所选省市区城市： " + province.getName() + " " + province.getId() + "\n" + city.getName()
                        + " " + city.getId() + "\n" + area.getName() + " " + area.getId() + "\n");
            }
        }
    }

}
