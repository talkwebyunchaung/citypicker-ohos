package com.talkweb.citypicker.slice;

import com.talkweb.citypicker.ResourceTable;
import com.talkweb.citypickerview.Interface.OnCityItemClickListener;
import com.talkweb.citypickerview.bean.CityBean;
import com.talkweb.citypickerview.bean.DistrictBean;
import com.talkweb.citypickerview.bean.ProvinceBean;
import com.talkweb.citypickerview.citywheel.CityConfig;
import com.talkweb.citypickerview.citywheel.CustomConfig;
import com.talkweb.citypickerview.style.citylist.Toast.ToastUtils;
import com.talkweb.citypickerview.style.citypickerview.CityPickerView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

public class CitypickerWheelAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    TextField mProEt;
    TextField mCityEt;
    TextField mAreaEt;
    TextField mProVisibleCountEt;

    Switch mProCyclicCk;
    Switch mCityCyclicCk;
    Switch mAreaCyclicCk;
    Switch mHalfBgCk;
    Switch mGATCk;

    Text mResultTv;
    Text mOneTv;
    Text mTwoTv;
    Text mThreeTv;

    private int visibleItems = 5;

    private boolean isProvinceCyclic = true;

    private boolean isCityCyclic = true;

    private boolean isDistrictCyclic = true;

    private boolean isShowBg = true;
    private boolean isShowGAT = true;

    private String defaultProvinceName = "江苏";

    private String defaultCityName = "常州";

    private String defaultDistrict = "新北区";

    public CityConfig.WheelType mWheelType = CityConfig.WheelType.PRO_CITY_DIS;

    CityPickerView mCityPickerView = new CityPickerView();
    private Text mSubmitTv;
    private Text mResetSettingTv;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_citypicker_wheel);

        findView();
        initListener();
        mCityPickerView.init(this);
    }

    private void initListener() {
        mOneTv.setClickedListener(this);
        mTwoTv.setClickedListener(this);
        mThreeTv.setClickedListener(this);
        mSubmitTv.setClickedListener(this);
        mOneTv.setClickedListener(this);
        mOneTv.setClickedListener(this);
    }

    private void findView() {
        mResultTv = (Text) findComponentById(ResourceTable.Id_resultTv);
        mProEt = (TextField) findComponentById(ResourceTable.Id_pro_et);
        mCityEt = (TextField) findComponentById(ResourceTable.Id_city_et);
        mAreaEt = (TextField) findComponentById(ResourceTable.Id_area_et);
        mProVisibleCountEt = (TextField) findComponentById(ResourceTable.Id_pro_visible_count_et);
        mProCyclicCk = (Switch) findComponentById(ResourceTable.Id_pro_cyclic_ck);
        mCityCyclicCk = (Switch) findComponentById(ResourceTable.Id_city_cyclic_ck);
        mAreaCyclicCk = (Switch) findComponentById(ResourceTable.Id_area_cyclic_ck);
        mGATCk = (Switch) findComponentById(ResourceTable.Id_gat_ck);
        mHalfBgCk = (Switch) findComponentById(ResourceTable.Id_half_bg_ck);
        mSubmitTv = (Text) findComponentById(ResourceTable.Id_customBtn);
        mResetSettingTv = (Text) findComponentById(ResourceTable.Id_reset_setting_tv);
        mOneTv = (Text) findComponentById(ResourceTable.Id_one_tv);
        mTwoTv = (Text) findComponentById(ResourceTable.Id_two_tv);
        mThreeTv = (Text) findComponentById(ResourceTable.Id_three_tv);

        //省份是否循环显示
        mProCyclicCk.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton buttonView, boolean isChecked) {
                isProvinceCyclic = isChecked;
            }
        });

        //市是否循环显示
        mCityCyclicCk.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton buttonView, boolean isChecked) {
                isCityCyclic = isChecked;
            }
        });

        //区是否循环显示
        mAreaCyclicCk.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton buttonView, boolean isChecked) {
                isDistrictCyclic = isChecked;
            }
        });

        //半透明背景显示
        mHalfBgCk.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                isShowBg = isChecked;
            }
        });

        //港澳台数据显示
        mGATCk.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                isShowGAT = isChecked;
            }
        });

        setWheelType(mWheelType);
    }

    /**
     * 重置属性
     */
    private void reset() {
        visibleItems = 5;
        isProvinceCyclic = true;
        isCityCyclic = true;
        isDistrictCyclic = true;
        isShowBg = true;
        isShowGAT = true;

        mWheelType = CityConfig.WheelType.PRO_CITY_DIS;

        setWheelType(mWheelType);

        mProCyclicCk.setChecked(true);
        mCityCyclicCk.setChecked(true);
        mAreaCyclicCk.setChecked(true);
        mGATCk.setChecked(true);

        mProEt.setText("" + defaultProvinceName);
        mCityEt.setText("" + defaultCityName);
        mAreaEt.setText("" + defaultDistrict);
        mProVisibleCountEt.setText("" + visibleItems);

        mHalfBgCk.setChecked(isShowBg);
        mProCyclicCk.setChecked(isProvinceCyclic);
        mAreaCyclicCk.setChecked(isDistrictCyclic);
        mCityCyclicCk.setChecked(isCityCyclic);
        mGATCk.setChecked(isShowGAT);

        setWheelType(mWheelType);

    }

    /**
     * @param wheelType
     */
    private void setWheelType(CityConfig.WheelType wheelType) {
        ShapeElement elementSelect = new ShapeElement(this, ResourceTable.Graphic_city_wheeltype_selected);
        ShapeElement elementNormal = new ShapeElement(this, ResourceTable.Graphic_city_wheeltype_normal);
        if (wheelType == CityConfig.WheelType.PRO) {
            mOneTv.setBackground(elementSelect);
            mTwoTv.setBackground(elementNormal);
            mThreeTv.setBackground(elementNormal);
            mOneTv.setTextColor(new Color(getColor(ResourceTable.Color_white)));
            mTwoTv.setTextColor(new Color(getColor(ResourceTable.Color_text_title)));
            mThreeTv.setTextColor(new Color(getColor(ResourceTable.Color_text_title)));
        } else if (wheelType == CityConfig.WheelType.PRO_CITY) {
            mOneTv.setBackground(elementNormal);
            mTwoTv.setBackground(elementSelect);
            mThreeTv.setBackground(elementNormal);
            mOneTv.setTextColor(new Color(getColor(ResourceTable.Color_text_title)));
            mTwoTv.setTextColor(new Color(getColor(ResourceTable.Color_white)));
            mThreeTv.setTextColor(new Color(getColor(ResourceTable.Color_text_title)));
        } else {
            mOneTv.setBackground(elementNormal);
            mTwoTv.setBackground(elementNormal);
            mThreeTv.setBackground(elementSelect);
            mOneTv.setTextColor(new Color(getColor(ResourceTable.Color_text_title)));
            mTwoTv.setTextColor(new Color(getColor(ResourceTable.Color_text_title)));
            mThreeTv.setTextColor(new Color(getColor(ResourceTable.Color_white)));
        }
    }

    /**
     * 弹出选择器
     */
    private void wheel() {
        defaultProvinceName = mProEt.getText();
        defaultCityName = mCityEt.getText();
        defaultDistrict = mAreaEt.getText();

        visibleItems = (Integer.parseInt(mProVisibleCountEt.getText()));

        CityConfig cityConfig = new CityConfig.Builder()
                .title("选择城市")
                .visibleItemsCount(visibleItems)
                .province(defaultProvinceName)
                .city(defaultCityName)
                .district(defaultDistrict)
                .provinceCyclic(isProvinceCyclic)
                .cityCyclic(isCityCyclic)
                .districtCyclic(isDistrictCyclic)
                .setCityWheelType(mWheelType)
                //自定义item的布局
                .setCustomItemLayout(ResourceTable.Layout_item_city)
                .setCustomItemTextViewId(ResourceTable.Id_item_city_name_tv)
                .setShowGAT(isShowGAT)
                .build();

        mCityPickerView.setConfig(cityConfig);
        mCityPickerView.setOnCityItemClickListener(new OnCityItemClickListener() {
            @Override
            public void onSelected(ProvinceBean province, CityBean city, DistrictBean district) {
                StringBuilder sb = new StringBuilder();
                sb.append("选择的结果：\n");
                if (province != null) {
                    sb.append(province.getName() + " " + province.getId() + "\n");
                }

                if (city != null) {
                    sb.append(city.getName() + " " + city.getId() + ("\n"));
                }

                if (district != null) {
                    sb.append(district.getName() + " " + district.getId() + ("\n"));
                }

                mResultTv.setText("" + sb.toString());
            }

            @Override
            public void onCancel() {
                //取消
            }
        });

        mCityPickerView.showCityPicker();
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_customBtn:
                //提交
                wheel();
                break;
            case ResourceTable.Id_reset_setting_tv:
                //重置属性
                reset();
                break;
            case ResourceTable.Id_one_tv:
                //一级联动，只显示省份，不显示市和区
                mWheelType = CityConfig.WheelType.PRO;
                setWheelType(mWheelType);
                break;
            case ResourceTable.Id_two_tv:
                //二级联动，只显示省份， 市，不显示区
                mWheelType = CityConfig.WheelType.PRO_CITY;
                setWheelType(mWheelType);
                break;
            case ResourceTable.Id_three_tv:
                //三级联动，显示省份， 市和区
                mWheelType = CityConfig.WheelType.PRO_CITY_DIS;
                setWheelType(mWheelType);
                break;
            default:

        }
    }
}
