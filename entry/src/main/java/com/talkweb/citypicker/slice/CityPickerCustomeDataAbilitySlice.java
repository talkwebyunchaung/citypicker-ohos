package com.talkweb.citypicker.slice;

import com.talkweb.citypickerview.Interface.OnCustomCityPickerItemClickListener;
import com.talkweb.citypickerview.bean.CustomCityData;
import com.talkweb.citypickerview.citywheel.CustomConfig;
import com.talkweb.citypickerview.style.citycustome.CustomCityPicker;
import com.talkweb.citypicker.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

import java.util.ArrayList;
import java.util.List;

public class CityPickerCustomeDataAbilitySlice extends AbilitySlice {

    private Text customeBtn;
    private Text resultTv;


    TextField mProVisibleCountEt;

    Switch mProCyclicCk;

    Switch mCityCyclicCk;

    Switch mAreaCyclicCk;

    Switch mHalfBgCk;
    Text mResetSettingTv;

    Text mOneTv;

    Text mTwoTv;

    Text mThreeTv;

    DirectionalLayout pro_cyclic_ll;
    DirectionalLayout city_cyclic_ll;
    DirectionalLayout area_cyclic_ll;

    private int visibleItems = 5;

    private boolean isProvinceCyclic = true;

    private boolean isCityCyclic = true;

    private boolean isDistrictCyclic = true;

    private boolean isShowBg = true;
    private CustomCityPicker customCityPicker = null;
    /**
     * 自定义数据源-省份数据
     */
    private List<CustomCityData> mProvinceListData = new ArrayList<>();
    public CustomConfig.WheelType mWheelType = CustomConfig.WheelType.PRO_CITY_DIS;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_city_picker_custome_data);

        initCustomeCityData();

        customCityPicker = new CustomCityPicker(this);
        pro_cyclic_ll = (DirectionalLayout) findComponentById(ResourceTable.Id_pro_cyclic_ll);
        city_cyclic_ll = (DirectionalLayout) findComponentById(ResourceTable.Id_city_cyclic_ll);
        area_cyclic_ll = (DirectionalLayout) findComponentById(ResourceTable.Id_area_cyclic_ll);
        customeBtn = (Text) findComponentById(ResourceTable.Id_customBtn);
        resultTv = (Text) findComponentById(ResourceTable.Id_resultTv);

        mProVisibleCountEt = (TextField) findComponentById(ResourceTable.Id_pro_visible_count_et);
        mProCyclicCk = (Switch) findComponentById(ResourceTable.Id_pro_cyclic_ck);
        mCityCyclicCk = (Switch) findComponentById(ResourceTable.Id_city_cyclic_ck);
        mAreaCyclicCk = (Switch) findComponentById(ResourceTable.Id_area_cyclic_ck);
        mHalfBgCk = (Switch) findComponentById(ResourceTable.Id_half_bg_ck);
        mResetSettingTv = (Text) findComponentById(ResourceTable.Id_reset_setting_tv);
        mOneTv = (Text) findComponentById(ResourceTable.Id_one_tv);
        mTwoTv = (Text) findComponentById(ResourceTable.Id_two_tv);
        mThreeTv = (Text) findComponentById(ResourceTable.Id_three_tv);


        setWheelType(mWheelType);
        //提交
        customeBtn.setClickedListener(v -> showView());

        //重置属性
        mResetSettingTv.setClickedListener(v -> reset());


        //一级联动，只显示省份，不显示市和区
        mOneTv.setClickedListener(v -> {
            mWheelType = CustomConfig.WheelType.PRO;
            setWheelType(mWheelType);
        });

        //二级联动，只显示省份， 市，不显示区
        mTwoTv.setClickedListener(v -> {
            mWheelType = CustomConfig.WheelType.PRO_CITY;
            setWheelType(mWheelType);
        });

        //三级联动，显示省份， 市和区
        mThreeTv.setClickedListener(v -> {
            mWheelType = CustomConfig.WheelType.PRO_CITY_DIS;
            setWheelType(mWheelType);
        });

        //省份是否循环显示
        mProCyclicCk.setCheckedStateChangedListener((absButton, isChecked) -> isProvinceCyclic = isChecked);

        //市是否循环显示
        mCityCyclicCk.setCheckedStateChangedListener((buttonView, isChecked) -> isCityCyclic = isChecked);

        //区是否循环显示
        mAreaCyclicCk.setCheckedStateChangedListener((buttonView, isChecked) -> isDistrictCyclic = isChecked);

        //半透明背景显示
        mHalfBgCk.setCheckedStateChangedListener((buttonView, isChecked) -> isShowBg = isChecked);
    }

    /**
     * 重置属性
     */
    private void reset() {
        visibleItems = 5;
        isProvinceCyclic = true;
        isCityCyclic = true;
        isDistrictCyclic = true;
        isShowBg = true;
        mWheelType = CustomConfig.WheelType.PRO_CITY_DIS;

        setWheelType(mWheelType);

        mProVisibleCountEt.setText("" + visibleItems);

        mHalfBgCk.setChecked(isShowBg);
        mProCyclicCk.setChecked(isProvinceCyclic);
        mAreaCyclicCk.setChecked(isDistrictCyclic);
        mCityCyclicCk.setChecked(isCityCyclic);

        setWheelType(mWheelType);

    }



    /**
     * @param wheelType
     */
    private void setWheelType(CustomConfig.WheelType wheelType) {
        ShapeElement elementSelect = new ShapeElement(this, ResourceTable.Graphic_city_wheeltype_selected);
        ShapeElement elementNormal = new ShapeElement(this, ResourceTable.Graphic_city_wheeltype_normal);
        if (wheelType == CustomConfig.WheelType.PRO) {
            mOneTv.setBackground(elementSelect);
            mTwoTv.setBackground(elementNormal);
            mThreeTv.setBackground(elementNormal);

            mOneTv.setTextColor(new Color(getColor(ResourceTable.Color_white)));
            mTwoTv.setTextColor(new Color(getColor(ResourceTable.Color_text_title)));
            mThreeTv.setTextColor(new Color(getColor(ResourceTable.Color_text_title)));

            pro_cyclic_ll.setVisibility(Component.VISIBLE);
            city_cyclic_ll.setVisibility(Component.HIDE);
            area_cyclic_ll.setVisibility(Component.HIDE);
        } else if (wheelType == CustomConfig.WheelType.PRO_CITY) {
            mOneTv.setBackground(elementNormal);
            mTwoTv.setBackground(elementSelect);
            mThreeTv.setBackground(elementNormal);
            mOneTv.setTextColor(new Color(getColor(ResourceTable.Color_text_title)));
            mTwoTv.setTextColor(new Color(getColor(ResourceTable.Color_white)));
            mThreeTv.setTextColor(new Color(getColor(ResourceTable.Color_text_title)));

            pro_cyclic_ll.setVisibility(Component.VISIBLE);
            city_cyclic_ll.setVisibility(Component.VISIBLE);
            area_cyclic_ll.setVisibility(Component.HIDE);
        } else {
            mOneTv.setBackground(elementNormal);
            mTwoTv.setBackground(elementNormal);
            mThreeTv.setBackground(elementSelect);
            mOneTv.setTextColor(new Color(getColor(ResourceTable.Color_text_title)));
            mTwoTv.setTextColor(new Color(getColor(ResourceTable.Color_text_title)));
            mThreeTv.setTextColor(new Color(getColor(ResourceTable.Color_white)));

            pro_cyclic_ll.setVisibility(Component.VISIBLE);
            city_cyclic_ll.setVisibility(Component.VISIBLE);
            area_cyclic_ll.setVisibility(Component.VISIBLE);
        }
    }

    private void showView() {
        CustomConfig cityConfig = new CustomConfig.Builder()
                .title("选择城市")
                .visibleItemsCount(visibleItems)
                .setCityData(mProvinceListData)
                .provinceCyclic(isProvinceCyclic)
                .province("浙江省")
                .city("宁波市")
                .district("鄞州区")
                .cityCyclic(isCityCyclic)
                //自定义item的布局
                .setCustomItemLayout(ResourceTable.Layout_item_custome_city)
                .setCustomItemTextViewId(ResourceTable.Id_item_custome_city_name_tv)
                .districtCyclic(isDistrictCyclic)
                .drawShadows(isShowBg)
                .setCityWheelType(mWheelType)
                .build();

        customCityPicker.setCustomConfig(cityConfig);
        customCityPicker.setOnCustomCityPickerItemClickListener(new OnCustomCityPickerItemClickListener() {
            @Override
            public void onSelected(CustomCityData province, CustomCityData city, CustomCityData district) {
                if (province != null && city != null && district != null) {
                    resultTv.setText("province：" + province.getName() + "    " + province.getId() + "\n" +
                            "city：" + city.getName() + "    " + city.getId() + "\n" +
                            "area：" + district.getName() + "    " + district.getId() + "\n");
                }else{
                    resultTv.setText("结果出错！");
                }
            }
        });
        customCityPicker.showCityPicker();
    }

    private void initCustomeCityData() {
        CustomCityData jsPro = new CustomCityData("10000", "江苏省");

        CustomCityData ycCity = new CustomCityData("11000", "盐城市");
        List<CustomCityData> ycDistList = new ArrayList<>();
        ycDistList.add(new CustomCityData("11100", "滨海县"));
        ycDistList.add(new CustomCityData("11200", "阜宁县"));
        ycDistList.add(new CustomCityData("11300", "大丰市"));
        ycDistList.add(new CustomCityData("11400", "盐都区"));
        ycCity.setList(ycDistList);

        CustomCityData czCity = new CustomCityData("12000", "常州市");
        List<CustomCityData> czDistList = new ArrayList<>();
        czDistList.add(new CustomCityData("12100", "新北区"));
        czDistList.add(new CustomCityData("12200", "天宁区"));
        czDistList.add(new CustomCityData("12300", "钟楼区"));
        czDistList.add(new CustomCityData("12400", "武进区"));
        czCity.setList(czDistList);

        List<CustomCityData> jsCityList = new ArrayList<>();
        jsCityList.add(ycCity);
        jsCityList.add(czCity);

        jsPro.setList(jsCityList);


        CustomCityData zjPro = new CustomCityData("20000", "浙江省");

        CustomCityData nbCity = new CustomCityData("21000", "宁波市");
        List<CustomCityData> nbDistList = new ArrayList<>();
        nbDistList.add(new CustomCityData("21100", "海曙区"));
        nbDistList.add(new CustomCityData("21200", "鄞州区"));
        nbCity.setList(nbDistList);

        CustomCityData hzCity = new CustomCityData("22000", "杭州市");
        List<CustomCityData> hzDistList = new ArrayList<>();
        hzDistList.add(new CustomCityData("22100", "上城区"));
        hzDistList.add(new CustomCityData("22200", "西湖区"));
        hzDistList.add(new CustomCityData("22300", "下沙区"));
        hzCity.setList(hzDistList);

        List<CustomCityData> zjCityList = new ArrayList<>();
        zjCityList.add(hzCity);
        zjCityList.add(nbCity);

        zjPro.setList(zjCityList);


        CustomCityData gdPro = new CustomCityData("30000", "广东省");

        CustomCityData fjCity = new CustomCityData("21000", "潮州市");
        List<CustomCityData> fjDistList = new ArrayList<>();
        fjDistList.add(new CustomCityData("21100", "湘桥区"));
        fjDistList.add(new CustomCityData("21200", "潮安区"));
        fjCity.setList(fjDistList);



        CustomCityData gzCity = new CustomCityData("22000", "广州市");
        List<CustomCityData> szDistList = new ArrayList<>();
        szDistList.add(new CustomCityData("22100", "荔湾区"));
        szDistList.add(new CustomCityData("22200", "增城区"));
        szDistList.add(new CustomCityData("22300", "从化区"));
        szDistList.add(new CustomCityData("22400", "南沙区"));
        szDistList.add(new CustomCityData("22500", "花都区"));
        szDistList.add(new CustomCityData("22600", "番禺区"));
        szDistList.add(new CustomCityData("22700", "黄埔区"));
        szDistList.add(new CustomCityData("22800", "白云区"));
        szDistList.add(new CustomCityData("22900", "天河区"));
        szDistList.add(new CustomCityData("22110", "海珠区"));
        szDistList.add(new CustomCityData("22120", "越秀区"));
        gzCity.setList(szDistList);

        List<CustomCityData> gdCityList = new ArrayList<>();
        gdCityList.add(gzCity);
        gdCityList.add(fjCity);

        gdPro.setList(gdCityList);


        mProvinceListData.add(jsPro);
        mProvinceListData.add(zjPro);
        mProvinceListData.add(gdPro);

    }

}
