package com.talkweb.citypicker.slice;

import com.talkweb.citypicker.ResourceTable;
import com.talkweb.citypickerview.Interface.OnCityItemClickListener;
import com.talkweb.citypickerview.bean.CityBean;
import com.talkweb.citypickerview.bean.DistrictBean;
import com.talkweb.citypickerview.bean.ProvinceBean;
import com.talkweb.citypickerview.style.cityjd.JDCityConfig;
import com.talkweb.citypickerview.style.cityjd.JDCityPicker;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

public class CitypickerJDAbilitySlice extends AbilitySlice {


    JDCityPicker cityPicker;
    private Button jdBtn;
    private Text resultV;
    Text mTwoTv;
    Text mThreeTv;

    public JDCityConfig.ShowType mWheelType = JDCityConfig.ShowType.PRO_CITY;


    private JDCityConfig jdCityConfig = new JDCityConfig.Builder().build();


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_citypicker_jd);

        jdBtn = (Button) findComponentById(ResourceTable.Id_jd_btn);
        resultV = (Text) findComponentById(ResourceTable.Id_result_tv);
        mTwoTv = (Text) findComponentById(ResourceTable.Id_two_tv);
        mThreeTv = (Text) findComponentById(ResourceTable.Id_three_tv);

        jdCityConfig.setShowType(mWheelType);

        //二级联动，只显示省份， 市，不显示区
        mTwoTv.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mWheelType = JDCityConfig.ShowType.PRO_CITY;
                setWheelType(mWheelType);
                jdCityConfig.setShowType(mWheelType);
            }
        });

        //三级联动，显示省份， 市和区
        mThreeTv.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mWheelType = JDCityConfig.ShowType.PRO_CITY_DIS;
                setWheelType(mWheelType);
                jdCityConfig.setShowType(mWheelType);
            }
        });


        cityPicker = new JDCityPicker();
        //初始化数据
        cityPicker.init(this);
        //设置JD选择器样式位只显示省份和城市两级
        cityPicker.setConfig(jdCityConfig);
        cityPicker.setOnCityItemClickListener(new OnCityItemClickListener() {
            @Override
            public void onSelected(ProvinceBean province, CityBean city, DistrictBean district) {

                String proData = null;
                if (province != null) {
                    proData = "name:  " + province.getName() + "   id:  " + province.getId();
                }

                String cituData = null;
                if (city != null) {
                    cituData = "name:  " + city.getName() + "   id:  " + city.getId();
                }


                String districtData = null;
                if (district != null) {
                    districtData = "name:  " + district.getName() + "   id:  " + district.getId();
                }


                if (mWheelType == JDCityConfig.ShowType.PRO_CITY_DIS) {
                    resultV.setText("城市选择结果：\n" + proData + "\n"
                            + cituData + "\n"
                            + districtData);
                } else {
                    resultV.setText("城市选择结果：\n" + proData + "\n"
                            + cituData + "\n"
                    );
                }
            }

            @Override
            public void onCancel() {
            }
        });

        jdBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                showJD();
            }
        });


    }


    /**
     * @param wheelType
     */
    private void setWheelType(JDCityConfig.ShowType wheelType) {

        ShapeElement element1 = new ShapeElement();
        element1.setRgbColor(new RgbColor(Color.BLUE.getValue()));
        ShapeElement element2 = new ShapeElement();
        element2.setRgbColor(new RgbColor(Color.DKGRAY.getValue()));

        if (wheelType == JDCityConfig.ShowType.PRO_CITY) {
          mTwoTv.setBackground(element1);
            mThreeTv.setBackground(element2);
            mTwoTv.setTextColor(AttrHelper.convertValueToColor("#ffffff"));
            mThreeTv.setTextColor(AttrHelper.convertValueToColor("#333333"));
        } else {
            mTwoTv.setBackground(element2);
            mThreeTv.setBackground(element1);
            mTwoTv.setTextColor(AttrHelper.convertValueToColor("#333333"));
            mThreeTv.setTextColor(AttrHelper.convertValueToColor("#ffffff"));
        }
    }


    private void showJD() {
        cityPicker.showCityPicker();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
